package com.example.rickmorty;

import android.graphics.Bitmap;

public class Character {
    public int id;
    public String name;
    public Bitmap image;

    Character(int id, String name, Bitmap image)
    {
        this.id=id;
        this.name=name;
        this.image=image;
    }

}
