package com.example.rickmorty;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public class TileAdapter extends BaseAdapter {
    private Context context;
    private Character[] tile;
    private LayoutInflater mInflator;

    TileAdapter(Context context ,Character [] tile)
    {
        this.context=context;
        this.tile=tile;
        mInflator = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return 100;//num of tiles
    }

    @Override
    public Object getItem(int position) {
        return this.tile[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = mInflator.inflate(R.layout.tile_details,null);
        return v;
    }
}
