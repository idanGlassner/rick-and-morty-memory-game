package com.example.rickmorty;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {
    ImageView IV;
    TextView TV;
    Character curr;
    int triesLeft;
    int[] locations;
    int pairCounter;
    int lastPos;
    Character[] charactersArray;
    GridView boardGV;
    TextView triesTV;
    Bitmap image;
    boolean nameTaken;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        boardGV=(GridView)findViewById(R.id.gridView);
        triesTV=(TextView)findViewById(R.id.textView);
        triesLeft=50;
        pairCounter=0;
        curr=null;
        charactersArray = new Character[100];
        locations = new int[100];
        int j;
        int tmp;
        for(int i = 0; i < locations.length; ++i)
        {locations[i] = i + 1;}
        /////////randomizes the {1-100} array/////////
        Random generator = new Random();
        for(int i = 0; i < locations.length; ++i) {
            j = generator.nextInt(locations.length - i);
            tmp = locations[locations.length - 1 - i];
            locations[locations.length - 1 - i] = locations[j];
            locations[j] = tmp;
        }
        //////////////////////////////////////////////
        try {
            parsejson();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        TileAdapter tileAdapter = new TileAdapter(this ,charactersArray);
        boardGV.setAdapter(tileAdapter);
        boardGV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (locations[position]==-1) {
                }else if (curr==null)
                {
                    curr=charactersArray[locations[position]];
                    TV=view.findViewById(R.id.tileTV);
                    IV=view.findViewById(R.id.tileIV);
                    TV.setText(curr.name);
                    IV.setScaleType(ImageView.ScaleType.FIT_XY);
                    IV.setImageBitmap(curr.image);
                    lastPos=position;
                }
                else {
                    if (position == lastPos) {
                    }else if(curr.id==charactersArray[locations[position]].id)
                    {
                        curr=charactersArray[locations[position]];
                        TV=view.findViewById(R.id.tileTV);
                        IV=view.findViewById(R.id.tileIV);
                        TV.setText(curr.name);
                        IV.setScaleType(ImageView.ScaleType.FIT_XY);
                        IV.setImageBitmap(curr.image);
                        pairCounter++;
                        if (pairCounter>=50)
                        {
                            AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                            alertDialog.setTitle("we have a winner");
                            alertDialog.setMessage("congratulations you have won the game with "+triesLeft+" tries to spare");
                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "thanks",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                            Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                                            startActivity(intent);
                                        }
                                    });
                            alertDialog.show();
                        }
                        locations[lastPos]=-1;
                        locations[position]=-1;
                        curr=null;
                    }
                    else
                    {
                        TV.setText("");
                        IV.setImageResource(R.drawable.rickandmorty);
                        triesLeft--;
                        triesTV.setText("you have "+triesLeft+" tries left");
                        if(triesLeft<=0)
                        {
                            AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                            alertDialog.setTitle("we have a looser");
                            alertDialog.setMessage("GAME OVER \nbetter luck next time (;");
                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "lets go again",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                            Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                                            startActivity(intent);
                                        }
                                    });
                            alertDialog.show();
                        }
                        curr=null;
                    }
                }
            }
        });
    }


    //it is meant to return the json from the given url as a string
    //but since i needed an AsyncTask to get the image from the url i used it for that too
    private class getJsonOrPic extends AsyncTask<String, String, String> {
        protected String doInBackground(String... params) {
            if (params[0].contains("jpeg"))
            {
                try {
                    image = BitmapFactory.decodeStream((InputStream)new URL(params[0]).getContent());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                }
                return buffer.toString();


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
    }

    //parses the json and creates the character array
    public void parsejson() throws JSONException, IOException {
        JSONObject obj;
        JSONArray characters;
        JSONObject person;
        String nextURL;
        URL url;
        String json = null;
        getJsonOrPic JsonOrPic;
        nextURL = "https://rickandmortyapi.com/api/character/";
        
        int i = 0;
        while (i < 100) {
            JsonOrPic = new getJsonOrPic();
            JsonOrPic.execute(nextURL);
            try {
                json = JsonOrPic.get();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            obj = new JSONObject(json);
            characters = obj.getJSONArray("results");
            for (int k = 0; k < characters.length() && i < 100; k++) {
                person = characters.getJSONObject(k);
                nameTaken = false;
                for (int j = 0; j < charactersArray.length && charactersArray[j] != null && !nameTaken; j++) {
                    if (charactersArray[j].name.equals(person.getString("name"))) {
                        nameTaken = true;
                    }
                }
                if (!nameTaken){
                    JsonOrPic = new getJsonOrPic();
                    JsonOrPic.execute(person.getString("image"));
                    try {
                        JsonOrPic.get();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    charactersArray[i] = new Character(person.getInt("id"), person.getString("name"), image);
                    charactersArray[i + 1] = charactersArray[i];
                    i += 2;
                }
            }
            nextURL = obj.getJSONObject("info").getString("next");
        }
    }
}



